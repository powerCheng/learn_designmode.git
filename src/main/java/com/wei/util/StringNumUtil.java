package com.wei.util;

import com.alibaba.fastjson.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author chengwei
 * @date 2019/1/23 15:57
 */
public class StringNumUtil {
    private static final String NUM_JSON = "{\"零\":0,\"幺\":1,\"一\":1,\"二\":2,\"三\":3,\"四\":4,\"五\":5,\"六\":6,\"七\":7,\"八\":8," +
            "\"九\":9}";
    private static final Map<String, Integer> CN_NUM_MAP;

    static {
        CN_NUM_MAP = JSONObject.parseObject(NUM_JSON, LinkedHashMap.class);
    }

    /**
     * 判断是否是大写数字
     */
    public static boolean isCnNumber(String str) {
        char[] nums = str.toCharArray();
        for (char num : nums) {
            if(!CN_NUM_MAP.containsKey(String.valueOf(num))){
                return false;
            }
        }
        return true;
    }

    /**
     * 将大写数字转换为阿拉伯数字
     */
    public static String getCnNumber(String str) {
        StringBuilder builder = new StringBuilder();
        char[] charNums = str.toCharArray();
        for (char c : charNums) {
            builder.append(String.valueOf(CN_NUM_MAP.get(String.valueOf(c))));
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        String str = "一二三四";
        if(isCnNumber(str)){
            System.out.println(getCnNumber(str));
        }
    }
}
