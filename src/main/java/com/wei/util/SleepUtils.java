package com.wei.util;

import java.util.concurrent.TimeUnit;

/**
 * @author chengwei
 * @date 2019/1/7 17:00
 */
public class SleepUtils {
    public static final void second(long seconds){
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
        }
    }

    public static final void milliSecond(long seconds){
        try {
            TimeUnit.MILLISECONDS.sleep(seconds);
        } catch (InterruptedException e) {
        }
    }
}
