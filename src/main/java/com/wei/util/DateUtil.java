package com.wei.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author chengwei
 * @date 2019/1/9 10:42
 */
public class DateUtil {
    public static String second(){
        return new SimpleDateFormat("mm:ss").format(new Date());
    }
}
