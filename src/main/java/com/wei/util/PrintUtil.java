package com.wei.util;

import org.apache.commons.lang.StringUtils;

/**
 * @author chengwei
 * @date 2019/1/18 14:48
 */
public class PrintUtil {

    public static void print(String str){
        System.out.print(str);
    }

    public static void println(String str){
        System.out.println(str);
    }

    public static void print(Integer str){
        System.out.print(str);
    }

    public static void println(Integer str){
        System.out.println(str);
    }

    public static void line(String str){
        System.out.println(StringUtils.center(str, 50, "-"));
    }

    public static void line(){
        line("");
    }
}
