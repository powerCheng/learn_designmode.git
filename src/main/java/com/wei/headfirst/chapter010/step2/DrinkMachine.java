package com.wei.headfirst.chapter010.step2;

/**
 * 自动饮料售卖机
 *
 * @author chengwei
 * @date 2019/8/28 14:21
 */
public class DrinkMachine {
    // 已售罄
    private State soldOutState;

    // 没有投币
    private State noCoinState;

    // 已投币
    private State hasCoinState;

    // 售卖成功
    private State soldState;

    // 中奖
    private State winnerState;

    // 创建售卖机时初始状态是没有饮料
    private State state = soldOutState;

    private int count = 0;

    private int winnerCount = 0;

    /**
     * 初始化售卖机放入饮料后, 状态是没有投币
     */
    public DrinkMachine(int count) {
        soldOutState = new SoldOutState(this);
        noCoinState = new NoCoinState(this);
        hasCoinState = new HasCoinState(this);
        soldState = new SoldState(this);
        winnerState = new WinnerState(this);
        this.count = count;
        if (count > 0) {
            state = noCoinState;
        }
    }

    /**
     * 客户投币
     */
    public void insertCoin() {
        state.insertCoin();
    }

    /**
     * 客户取消购买, 撤回硬币
     */
    public void ejectCoin() {
        state.ejectCoin();
    }

    /**
     * 客户转动曲柄, 确认购买
     */
    public void turnCrank() {
        state.turnCrank();
        state.dispense();
    }

    /**
     * 发放饮料
     */
    private void dispense() {
        state.dispense();
    }

    public State getSoldOutState() {
        return soldOutState;
    }

    public State getNoCoinState() {
        return noCoinState;
    }

    public State getHasCoinState() {
        return hasCoinState;
    }

    public State getSoldState() {
        return soldState;
    }

    public State getWinnerState() {
        return winnerState;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getWinnerCount() {
        return winnerCount;
    }

    public void setWinnerCount(int winnerCount) {
        this.winnerCount = winnerCount;
    }

    @Override
    public String toString() {
        return "DrinkMachine{" +
                "state=" + state.getClass().getSimpleName() +
                ", count=" + count +
                '}';
    }
}
