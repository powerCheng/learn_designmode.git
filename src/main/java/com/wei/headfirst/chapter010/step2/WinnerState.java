package com.wei.headfirst.chapter010.step2;

/**
 * @author chengwei
 * @date 2019/8/28 16:10
 */
public class WinnerState extends State {
    private DrinkMachine drinkMachine;

    public WinnerState(DrinkMachine drinkMachine) {
        this.drinkMachine = drinkMachine;
    }

    @Override
    public void insertCoin() {
        System.out.println("投币失败, 饮料正在出库, 请稍后再投");
    }

    @Override
    public void ejectCoin() {
        System.out.println("抱歉, 饮料正在出库, 无法退回");
    }

    @Override
    public void turnCrank() {
        System.out.println("请勿重复转动曲柄, 饮料正在出库");
    }

    @Override
    public void dispense() {
        System.out.println("恭喜你, 中了一瓶饮料, 两瓶饮料发放成功");
        drinkMachine.setCount(drinkMachine.getCount() -2);
        drinkMachine.setWinnerCount(drinkMachine.getWinnerCount()+1);
        if (drinkMachine.getCount() <= 0){
            drinkMachine.setState(drinkMachine.getSoldOutState());
        }else {
            drinkMachine.setState(drinkMachine.getNoCoinState());
        }
    }
}
