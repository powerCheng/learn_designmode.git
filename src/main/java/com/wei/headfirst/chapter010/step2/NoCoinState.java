package com.wei.headfirst.chapter010.step2;

/**
 * @author chengwei
 * @date 2019/8/28 16:04
 */
public class NoCoinState extends State {

    private DrinkMachine drinkMachine;

    public NoCoinState(DrinkMachine drinkMachine) {
        this.drinkMachine = drinkMachine;
    }

    @Override
    public void insertCoin() {
        drinkMachine.setState(drinkMachine.getHasCoinState());
        System.out.println("投币成功");
    }

    @Override
    public void ejectCoin() {
        System.out.println("抱歉, 您尚未投币不能退回");
    }

    @Override
    public void turnCrank() {
        System.out.println("抱歉, 你尚未投币, 无法购买");
    }
}
