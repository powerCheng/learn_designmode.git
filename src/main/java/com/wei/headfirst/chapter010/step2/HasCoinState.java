package com.wei.headfirst.chapter010.step2;

import org.apache.commons.lang.math.RandomUtils;

/**
 * @author chengwei
 * @date 2019/8/28 16:09
 */
public class HasCoinState extends State {
    private DrinkMachine drinkMachine;

    public HasCoinState(DrinkMachine drinkMachine) {
        this.drinkMachine = drinkMachine;
    }

    @Override
    public void insertCoin() {
        System.out.println("投币失败, 投币口已有硬币");
    }

    @Override
    public void ejectCoin() {
        drinkMachine.setState(drinkMachine.getNoCoinState());
        System.out.println("硬币退回成功");
    }

    @Override
    public void turnCrank() {
        System.out.println("购买成功, 饮料正在出库");
        if(RandomUtils.nextInt(10) == 0 && drinkMachine.getCount() >= 2){
            drinkMachine.setState(drinkMachine.getWinnerState());
        }else {
            drinkMachine.setState(drinkMachine.getSoldState());
        }
    }

}
