package com.wei.headfirst.chapter010.step2;


import static com.wei.util.PrintUtil.line;

/**
 * @author chengwei
 * @date 2019/8/28 14:43
 */
public class Client {
    public static void main(String[] args){
        DrinkMachine machine = new DrinkMachine(500);
        for (int i = 0; i < 500; i++) {
            machine.insertCoin();
            machine.turnCrank();
            line();
        }
        line();
        System.out.println("中奖人次共计: "+ machine.getWinnerCount());
    }
}
