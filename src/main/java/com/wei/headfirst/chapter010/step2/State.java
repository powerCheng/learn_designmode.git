package com.wei.headfirst.chapter010.step2;

/**
 * 状态类接口
 *
 * @author chengwei
 * @date 2019/8/28 16:01
 */
public abstract class State {

    /**
     * 客户投币
     */
    void insertCoin(){}

    /**
     * 客户取消购买, 撤回硬币
     */
    void ejectCoin(){}

    /**
     * 客户转动曲柄, 确认购买
     */
    void turnCrank(){}

    /**
     * 发放饮料
     */
    void dispense(){}
}
