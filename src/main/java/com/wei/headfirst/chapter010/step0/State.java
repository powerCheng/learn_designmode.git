package com.wei.headfirst.chapter010.step0;

/**
 * 状态接口
 *
 * @author chengwei
 * @date 2019/8/29 10:32
 */
public interface State {

    /**
     * 行为1
     */
    void handler1();

    /**
     * 行为2
     */
    void handler2();

}
