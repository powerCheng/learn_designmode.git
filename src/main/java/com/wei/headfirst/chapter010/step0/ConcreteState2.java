package com.wei.headfirst.chapter010.step0;

/**
 * @author chengwei
 * @date 2019/8/29 10:33
 */
public class ConcreteState2 implements State {

    private Context context;

    public ConcreteState2(Context context) {
        this.context = context;
    }

    @Override
    public void handler1() {
        System.out.println("状态2情况下执行行为1");
    }

    @Override
    public void handler2() {
        System.out.println("状态2情况下执行行为2");
        context.setState(context.getConcreteState1());
    }
}
