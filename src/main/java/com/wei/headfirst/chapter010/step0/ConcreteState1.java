package com.wei.headfirst.chapter010.step0;

/**
 * @author chengwei
 * @date 2019/8/29 10:33
 */
public class ConcreteState1 implements State {

    private Context context;

    public ConcreteState1(Context context) {
        this.context = context;
    }

    @Override
    public void handler1() {
        System.out.println("状态1情况下执行行为1");
        context.setState(context.getConcreteState2());
    }

    @Override
    public void handler2() {
        System.out.println("状态1情况下执行行为2");
    }
}
