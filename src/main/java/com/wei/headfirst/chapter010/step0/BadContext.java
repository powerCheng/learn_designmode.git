package com.wei.headfirst.chapter010.step0;

/**
 * @author chengwei
 * @date 2019/8/29 19:54
 */
public class BadContext {
    private Integer state;

    public BadContext() {
        state = 1;
    }

    private void handler1() {
        if (state == 1) {
            System.out.println("...");
            state = 2;
        } else if (state == 2) {
            System.out.println("...");
        } else if (state == 3) {
            System.out.println("...");
        }
    }

    private void handler2() {
        if (state == 1) {
            System.out.println("===");
        } else if (state == 2) {
            System.out.println("===");
        } else if (state == 3) {
            System.out.println("===");
        }
    }
}


