package com.wei.headfirst.chapter010.step0;

/**
 * @author chengwei
 * @date 2019/8/29 16:09
 */
public class Client {
    public static void main(String[] args) {
        Context context = new Context();
        context.handler1();
        context.handler2();
        context.handler2();
        context.handler1();
        context.handler1();
    }
}
