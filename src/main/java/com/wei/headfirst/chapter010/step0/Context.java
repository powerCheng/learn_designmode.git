package com.wei.headfirst.chapter010.step0;

/**
 * 环境类
 *
 * @author chengwei
 * @date 2019/8/29 10:11
 */
public class Context {
    /**
     * 状态: 会影响行为的结果
     */
    private State state;

    /**
     * 具体的状态1
     */
    private State concreteState1;

    /**
     * 具体的状态2
     */
    private State concreteState2;

    public Context() {
        concreteState1 = new ConcreteState1(this);
        concreteState2 = new ConcreteState2(this);
        state = concreteState1;
    }

    /**
     * 行为1
     */
    public void handler1(){
        state.handler1();
    }

    /**
     * 行为2
     */
    public void handler2(){
        state.handler2();
    }

    public State getConcreteState1() {
        return concreteState1;
    }

    public State getConcreteState2() {
        return concreteState2;
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }
}
