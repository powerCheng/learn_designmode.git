package com.wei.headfirst.chapter010.step1;

/**
 * 自动饮料售卖机
 *
 * @author chengwei
 * @date 2019/8/28 14:21
 */
public class DrinkMachine {
    // 已售罄
    private static final int SOLD_OUT = 0;

    // 没有投币
    private static final int NO_COIN = 1;

    // 已投币
    private static final int HAS_COIN = 2;

    // 售卖成功
    private static final int SOLD = 3;

    // 创建售卖机时初始状态是没有饮料
    private int state = SOLD_OUT;

    private int count = 0;

    /**
     * 初始化售卖机放入饮料后, 状态是没有投币
     */
    public DrinkMachine(int count) {
        this.count = count;
        if(count > 0){
            state = NO_COIN;
        }
    }

    /**
     * 客户投币
     */
    public void insertCoin(){
        if(state == NO_COIN){
            System.out.println("投币成功");
            state = HAS_COIN;
        }else if (state == HAS_COIN){
            System.out.println("投币失败, 投币口已有硬币");
        }else if (state == SOLD_OUT){
            System.out.println("投币失败, 饮料已售罄");
        }else if (state == SOLD){
            System.out.println("投币失败, 饮料正在出库, 请稍后再投");
        }
    }

    /**
     * 客户取消购买, 撤回硬币
     */
    public void ejectCoin(){
        if(state == NO_COIN){
            System.out.println("抱歉, 您尚未投币不能退回");
        }else if (state == HAS_COIN){
            state = NO_COIN;
            System.out.println("硬币退回成功");
        }else if (state == SOLD_OUT){
            System.out.println("抱歉, 您尚未投币不能退回");
        }else if (state == SOLD){
            System.out.println("抱歉, 饮料正在出库, 无法退回");
        }
    }

    /**
     * 客户转动曲柄, 确认购买
     */
    public void turnCrank(){
        if(state == NO_COIN){
            System.out.println("抱歉, 你尚未投币, 无法购买");
        }else if (state == HAS_COIN){
            System.out.println("购买成功, 饮料正在出库");
            state = SOLD;
            dispense();
        }else if (state == SOLD_OUT){
            System.out.println("抱歉, 你尚未投币, 无法购买");
        }else if (state == SOLD){
            System.out.println("请勿重复转动曲柄, 饮料正在出库");
        }
    }

    /**
     * 发放饮料
     */
    private void dispense(){
        try {
            if (state == SOLD){
                Thread.sleep(1000);
                System.out.println("饮料发放成功");
                count--;
                if (count <= 0){
                    state = SOLD_OUT;
                }else {
                    state = NO_COIN;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "DrinkMachine{" +
                "state=" + state +
                ", count=" + count +
                '}';
    }
}
