package com.wei.headfirst.chapter010.step1;

import static com.wei.util.PrintUtil.line;

/**
 * @author chengwei
 * @date 2019/8/28 14:43
 */
public class Client {
    public static void main(String[] args) throws InterruptedException {
        DrinkMachine machine = new DrinkMachine(5);
        machine.insertCoin();
        new Thread(()->{
            machine.turnCrank();
            machine.ejectCoin();
        }).start();
        machine.turnCrank();
        line();
        machine.insertCoin();
        machine.ejectCoin();
        line();
        machine.insertCoin();
        machine.turnCrank();
        System.out.println(machine);
        machine.ejectCoin();
        machine.turnCrank();
        line();
        machine.insertCoin();
        machine.turnCrank();
        line();
        System.out.println(machine);
        machine.insertCoin();
        machine.ejectCoin();
        System.out.println(machine);
        machine.insertCoin();
        machine.turnCrank();
        line();
        machine.insertCoin();
        machine.turnCrank();
        line();
        machine.insertCoin();
        machine.turnCrank();
        line();
    }
}
