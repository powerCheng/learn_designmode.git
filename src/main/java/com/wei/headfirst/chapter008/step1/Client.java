package com.wei.headfirst.chapter008.step1;

import static com.wei.util.PrintUtil.line;

/**
 * @author chengwei
 * @date 2019/2/13 9:45
 */
public class Client {
    public static void main(String[] args) {
        Drink tea = new Tea();
        Drink coffee = new Coffee();
        tea.prepareRecipe();
        line();
        coffee.prepareRecipe();
    }
}
