package com.wei.headfirst.chapter008.step1;

/**
 * @author chengwei
 * @date 2019/2/13 9:41
 */
public class Coffee extends Drink {

    @Override
    void brew() {
        System.out.println("冲泡咖啡");
    }

    @Override
    void addCondiments() {
        System.out.println("加入糖和牛奶");
    }
}
