package com.wei.headfirst.chapter008.step1;

/**
 * 饮料, 模板类
 *
 * @author chengwei
 * @date 2019/2/13 9:38
 */
public abstract class Drink {

    /**
     * 准备过程
     */
    public final void prepareRecipe() {
        // 烧水
        boilWater();
        // 冲泡手法
        brew();
        // 加调料
        addCondiments();
        // 倒饮料倒进杯子
        pourInCup();
    }

    void boilWater() {
        System.out.println("烧水");
    }

    void pourInCup() {
        System.out.println("把水倒入杯子中");
    }

    abstract void brew();

    abstract void addCondiments();
}
