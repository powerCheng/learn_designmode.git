package com.wei.headfirst.chapter008.step1;

/**
 * @author chengwei
 * @date 2019/2/13 9:45
 */
public class Tea extends Drink {
    @Override
    void brew() {
        System.out.println("冲泡茶叶");
    }

    @Override
    void addCondiments() {
        System.out.println("加入柠檬");
    }
}
