package com.wei.headfirst.chapter004.step0;

import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 *
 * @author chengwei
 * @date 2019/8/26 16:09
 */
public class OperationFactory2 {
    private volatile static List<Operation> list = null;

    public static Operation getOperation(String type){
        for (Operation operation : getOperations()) {
            if(type.equals(operation.getType())){
                return operation;
            }
        }
        return null;
    }

    private static List<Operation> getOperations(){
        try {
            if(list == null){
                synchronized (OperationFactory2.class){
                    if(list == null){
                        list = new ArrayList<>();
                        Reflections reflections = new Reflections(Operation.class.getPackage().getName());
                        Set<Class<? extends Operation>> operations = reflections.getSubTypesOf(Operation.class);
                        for (Class<? extends Operation> operationClazz : operations) {
                            Operation operation = operationClazz.newInstance();
                            list.add(operation);
                        }
                    }
                }
            }
        } catch (InstantiationException | IllegalAccessException e ) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return list;
    }
}
