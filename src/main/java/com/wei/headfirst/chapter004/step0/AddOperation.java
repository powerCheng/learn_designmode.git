package com.wei.headfirst.chapter004.step0;

public class AddOperation implements Operation {
    @Override
    public int operation(int n1, int n2) {
        return n1 + n2;
    }

    @Override
    public String getType() {
        return "+";
    }
}
