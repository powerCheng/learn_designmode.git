package com.wei.headfirst.chapter004.step0;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    public static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        int n1 = 8;
        int n2 = 2;
        String operate = "+";

        Operation operation = OperationFactory2.getOperation(operate);
        int result = operation.operation(n1, n2);
        logger.info("{} {} {} = {}", n1, operate, n2, result);
    }
}
