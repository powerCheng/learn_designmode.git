package com.wei.headfirst.chapter004.step0;

/**
 * 简单工厂
 *
 * @author chengwei
 * @date 2019/8/26 15:40
 */
public class OperationFactory {

    public static Operation instanceOperation(String symbol) {
        switch (symbol) {
            case "+":
                return new AddOperation();
            case "-":
                return new SubtractOperation();
            case "*":
                return new MultiplyOperation();
            case "/":
                return new DividOperation();
            default:
                return new DefaultOperation();
        }
    }
}
