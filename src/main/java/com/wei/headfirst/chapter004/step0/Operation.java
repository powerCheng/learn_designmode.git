package com.wei.headfirst.chapter004.step0;

public interface Operation {
    int operation(int n1, int n2);

    String getType();
}
