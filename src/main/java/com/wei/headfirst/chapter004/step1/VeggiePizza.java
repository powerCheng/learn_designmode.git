package com.wei.headfirst.chapter004.step1;

/**
 * 蔬菜披萨
 */
public class VeggiePizza extends Pizza {

    public VeggiePizza() {
        name = "蔬菜披萨";
    }
}
