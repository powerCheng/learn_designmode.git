package com.wei.headfirst.chapter004.step1;

/**
 * @author chengwei
 * @date 2019/8/26 17:52
 */
public class Client {
    public static void main(String[] args) {
        Pizza pizza = PizzaStore.createPizza("cheese");
        System.out.println(pizza.name);
    }
}
