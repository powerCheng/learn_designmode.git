package com.wei.headfirst.chapter004.step1;


/**
 * 披萨店
 */
public class PizzaStore {

    public static Pizza createPizza(String type) {
        switch (type) {
            // 奶酪披萨
            case "cheese":
                return new CheesePizza();
            // 蔬菜披萨
            case "veggie":
                return new VeggiePizza();
            // 默认披萨
            default:
                return new DefaultPizza();
        }
    }
}
