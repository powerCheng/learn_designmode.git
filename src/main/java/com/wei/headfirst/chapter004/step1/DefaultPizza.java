package com.wei.headfirst.chapter004.step1;


public class DefaultPizza extends Pizza {
    @Override
    void prepare() {

    }

    @Override
    void bake() {

    }

    @Override
    void cut() {

    }

    @Override
    void box() {

    }
}
