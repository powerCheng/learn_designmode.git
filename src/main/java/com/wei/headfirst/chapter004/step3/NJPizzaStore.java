package com.wei.headfirst.chapter004.step3;

/**
 * 南京披萨店
 */
public class NJPizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(String type) {
        switch (type) {
            case "cheese":
                return new NJCheesePizza();
            case "veggie":
                return new NJVeggiePizza();
            default:
                return new DefaultPizza();
        }
    }
}
