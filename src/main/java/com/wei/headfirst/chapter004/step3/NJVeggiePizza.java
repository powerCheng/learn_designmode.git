package com.wei.headfirst.chapter004.step3;

/**
 * 南京蔬菜披萨
 */
public class NJVeggiePizza extends Pizza {

    public NJVeggiePizza() {
        name = "南京蔬菜披萨";
    }
}
