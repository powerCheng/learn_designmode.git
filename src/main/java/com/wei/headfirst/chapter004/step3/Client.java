package com.wei.headfirst.chapter004.step3;

import static com.wei.util.PrintUtil.line;

public class Client {

    private PizzaStore pizzaStore;

    public Client(PizzaStore pizzaStore) {
        this.pizzaStore = pizzaStore;
    }

    public Pizza orderPizza(String type){
        return pizzaStore.orderPizza(type);
    }

    public static void main(String[] args) {
        Client client = new Client(new SZPizzaStore());
//        Client client = new Client(new NJPizzaStore());
        client.orderPizza("veggie");
        line();
        client.orderPizza("cheese");
    }
}
