package com.wei.headfirst.chapter004.step3;

/**
 * 苏州披萨店
 */
public class SZPizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(String type) {
        switch (type) {
            case "cheese":
                return new SZCheesePizza();
            case "veggie":
                return new SZVeggiePizza();
            default:
                return new DefaultPizza();
        }
    }
}
