package com.wei.headfirst.chapter004.step3;

/**
 * 披萨店抽象类
 */
public abstract class PizzaStore {

    /**
     * 这个地方使用的是模板方法模式, 披萨各子类的制作流程不同, 但是准备/烘烤/切割/包装的步骤是一样的
     */
    public Pizza orderPizza(String type){
        Pizza pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }

    /**
     * 这是工厂方法的关键
     */
    protected abstract Pizza createPizza(String type);
}
