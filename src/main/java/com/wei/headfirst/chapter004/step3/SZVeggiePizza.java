package com.wei.headfirst.chapter004.step3;

public class SZVeggiePizza extends Pizza {
    public SZVeggiePizza() {
        name = "苏州蔬菜披萨";
    }

    @Override
    void bake() {
        System.out.println("使用苏州的方式烘烤蔬菜披萨中...");
    }
}
