package com.wei.headfirst.chapter004.step3;

/**
 * 披萨父类
 */
public abstract class Pizza {

    protected String name;

    void prepare(){
        System.out.println("正在准备: " + name);
    }

    void bake(){
        System.out.println("烘烤中...");
    }

    void cut(){
        System.out.println("切割中...");
    }

    void box(){
        System.out.println("包装中...");
    }

}
