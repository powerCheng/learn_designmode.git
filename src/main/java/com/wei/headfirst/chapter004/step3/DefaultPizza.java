package com.wei.headfirst.chapter004.step3;

public class DefaultPizza extends Pizza {
    @Override
    void prepare() {

    }

    @Override
    void bake() {

    }

    @Override
    void cut() {

    }

    @Override
    void box() {

    }
}
