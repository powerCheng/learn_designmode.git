package com.wei.headfirst.chapter004.step3;

public class NJCheesePizza extends Pizza {

    public NJCheesePizza() {
        name = "南京奶酪披萨";
    }

    @Override
    void bake() {
        System.out.println("使用南京的方式烘烤奶酪披萨中...");
    }
}
