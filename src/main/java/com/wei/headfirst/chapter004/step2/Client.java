package com.wei.headfirst.chapter004.step2;

public class Client {
    private Factory factory;

    public Client(Factory factory) {
        this.factory = factory;
    }

    public Product instanceProduct(){
        return factory.instaceProduct();
    }
}
