package com.wei.headfirst.chapter004.step2;

public class Main {
    public static void main(String[] args) {
        Client client = new Client(new ConcreteFactory1());
        Product product = client.instanceProduct();
        product.show();
    }
}
