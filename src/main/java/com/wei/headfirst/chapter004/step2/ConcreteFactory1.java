package com.wei.headfirst.chapter004.step2;

public class ConcreteFactory1 implements Factory {
    @Override
    public Product instaceProduct() {
        return new ConcreteProduct1();
    }
}
