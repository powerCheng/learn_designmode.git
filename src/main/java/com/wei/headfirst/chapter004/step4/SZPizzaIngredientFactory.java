package com.wei.headfirst.chapter004.step4;

/**
 * 苏州原料工厂
 *
 * @author chengwei
 * @date 2019/8/26 20:06
 */
public class SZPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new CornDough();
    }

    @Override
    public Sauce createSauce() {
        return new SweetSauce();
    }
}
