package com.wei.headfirst.chapter004.step4;

/**
 * 披萨父类
 */
public abstract class Pizza {

    protected String name;

    /**
     * 面料
     */
    protected Dough dough;

    /**
     * 酱料
     */
    protected Sauce sauce;

    /**
     * 原料工厂
     */
    private PizzaIngredientFactory pizzaIngredientFactory;

    public Pizza() {
    }

    public Pizza(PizzaIngredientFactory pizzaIngredientFactory) {
        this.pizzaIngredientFactory = pizzaIngredientFactory;
    }

    void prepare(){
        System.out.println("正在准备: " + name);
        dough = pizzaIngredientFactory.createDough();
        sauce = pizzaIngredientFactory.createSauce();
    }

    void bake(){
        System.out.println("烘烤中...");
    }

    void cut(){
        System.out.println("切割中...");
    }

    void box(){
        System.out.println("包装中...");
    }

    void printMeterial(){
        System.out.println(name +": "+ dough+", "+sauce);
    }

}
