package com.wei.headfirst.chapter004.step4;

/**
 * 甜酱
 *
 * @author chengwei
 * @date 2019/8/26 20:01
 */
public class SweetSauce extends Sauce {
    public SweetSauce() {
        name = "甜酱";
    }
}
