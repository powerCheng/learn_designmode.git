package com.wei.headfirst.chapter004.step4;

/**
 * 小麦面粉
 *
 * @author chengwei
 * @date 2019/8/26 19:56
 */
public class WheatDough extends Dough {

    public WheatDough() {
        name = "小麦面粉";
    }
}
