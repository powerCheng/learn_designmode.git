package com.wei.headfirst.chapter004.step4;

/**
 * 南京原料工厂
 *
 * @author chengwei
 * @date 2019/8/26 20:05
 */
public class NJPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new WheatDough();
    }

    @Override
    public Sauce createSauce() {
        return new TomatoSauce();
    }
}
