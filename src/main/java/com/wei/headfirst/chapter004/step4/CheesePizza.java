package com.wei.headfirst.chapter004.step4;

/**
 * @author chengwei
 * @date 2019/8/26 20:10
 */
public class CheesePizza extends Pizza {

    public CheesePizza(PizzaIngredientFactory pizzaIngredientFactory) {
        super(pizzaIngredientFactory);
        name = "奶酪披萨";
    }
}
