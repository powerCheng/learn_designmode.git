package com.wei.headfirst.chapter004.step4;


/**
 * 苏州披萨店
 */
public class SZPizzaStore extends PizzaStore {
    private PizzaIngredientFactory ingredientFactory;

    public SZPizzaStore(PizzaIngredientFactory ingredientFactory) {
        this.ingredientFactory = ingredientFactory;
    }

    @Override
    protected Pizza createPizza(String type) {
        switch (type) {
            case "cheese":
                return new CheesePizza(ingredientFactory);
            case "veggie":
                return new VeggiePizza(ingredientFactory);
            default:
                return null;
        }
    }
}
