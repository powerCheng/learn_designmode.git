package com.wei.headfirst.chapter004.step4;

/**
 * @author chengwei
 * @date 2019/8/26 20:19
 */
public class Client {
    public static void main(String[] args) {
        PizzaIngredientFactory njIngredientFactory = new NJPizzaIngredientFactory();
        PizzaIngredientFactory szIngredientFactory = new SZPizzaIngredientFactory();
        PizzaStore njStore = new NJPizzaStore(njIngredientFactory);
        PizzaStore szStore = new SZPizzaStore(szIngredientFactory);
        Pizza njCheesePizza = njStore.orderPizza("cheese");
        Pizza szCheesePizza = szStore.orderPizza("cheese");
        njCheesePizza.printMeterial();
        szCheesePizza.printMeterial();
    }
}
