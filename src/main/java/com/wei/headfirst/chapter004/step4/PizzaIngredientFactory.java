package com.wei.headfirst.chapter004.step4;

/**
 * 披萨原料工厂
 *
 * @author chengwei
 * @date 2019/8/26 19:49
 */
public interface PizzaIngredientFactory {

    /**
     * 创建面料
     */
    Dough createDough();

    /**
     * 创建酱料
     */
    Sauce createSauce();
}
