package com.wei.headfirst.chapter004.step4;


/**
 * 南京披萨店
 */
public class NJPizzaStore extends PizzaStore {

    private PizzaIngredientFactory ingredientFactory;

    public NJPizzaStore(PizzaIngredientFactory ingredientFactory) {
        this.ingredientFactory = ingredientFactory;
    }

    @Override
    protected Pizza createPizza(String type) {
        switch (type) {
            case "cheese":
                return new CheesePizza(ingredientFactory);
            case "veggie":
                return new VeggiePizza(ingredientFactory);
            default:
                return null;
        }
    }
}
