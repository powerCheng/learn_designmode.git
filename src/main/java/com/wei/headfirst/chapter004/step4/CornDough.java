package com.wei.headfirst.chapter004.step4;

/**
 * 玉米面粉
 *
 * @author chengwei
 * @date 2019/8/26 19:58
 */
public class CornDough extends Dough {

    public CornDough() {
        name = "玉米面粉";
    }
}
