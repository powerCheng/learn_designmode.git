package com.wei.headfirst.chapter004.step4;

/**
 * 番茄酱
 *
 * @author chengwei
 * @date 2019/8/26 20:00
 */
public class TomatoSauce extends Sauce {
    public TomatoSauce() {
        name = "番茄酱";
    }
}
