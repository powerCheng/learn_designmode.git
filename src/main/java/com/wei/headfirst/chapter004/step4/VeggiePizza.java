package com.wei.headfirst.chapter004.step4;

/**
 * @author chengwei
 * @date 2019/8/26 20:14
 */
public class VeggiePizza extends Pizza {
    public VeggiePizza(PizzaIngredientFactory pizzaIngredientFactory) {
        super(pizzaIngredientFactory);
        name = "蔬菜披萨";
    }
}
