package com.wei.headfirst.chapter005.step1;

/**
 * 单例的饿汉模式
 *
 * @author chengwei
 * @date 2019-06-06 10:01
 */
public class Singleton {
    /** 当加载Singleton.class时创建该对象 */
    private static Singleton singleton = new Singleton();

    /** 注意将构造器改为私有的 */
    private Singleton() {
    }

    public static Singleton getInstance() {
        return singleton;
    }
}
