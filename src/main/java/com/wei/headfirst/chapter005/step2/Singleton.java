package com.wei.headfirst.chapter005.step2;

/**
 * 单例线程安全的懒汉模式
 *
 * @author chengwei
 * @date 2019-06-06 10:04
 */
public class Singleton {
    /** 私有的 */
    private volatile static Singleton singleton;

    /** 私有的 */
    private Singleton() {
    }

    private static Singleton getInstance() {
        if (null == singleton) {
            synchronized (Singleton.class) {
                // 获取锁后再判断下是否为null, 有可能在等待锁的时候上一个线程已经创建了对象
                if(null == singleton){
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }
}
