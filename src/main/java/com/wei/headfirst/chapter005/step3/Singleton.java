package com.wei.headfirst.chapter005.step3;

/**
 * 单例线程安全的懒汉模式
 *
 * @author chengwei
 * @date 2019-06-06 10:08
 */
public class Singleton {

    /** 私有的 */
    private Singleton() {
    }

    /** 私有的静态内部类 */
    private static class SingletonFactory {
        private static Singleton instance = new Singleton();
    }

    public static Singleton getInstance() {
        // 根据JVM加载机制, 第一次使用某个类的时候才会去加载这个类.
        // 所以只有当调用getInstance()方法时类SingletonFactory才会被加载, 同时instance对象将会被创建
        return SingletonFactory.instance;
    }
}
