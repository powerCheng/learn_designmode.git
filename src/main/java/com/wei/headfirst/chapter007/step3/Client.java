package com.wei.headfirst.chapter007.step3;

/**
 * @author chengwei
 * @date 2019/8/27 15:54
 */
public class Client {
    public static void main(String[] args) {
        Facade facade = new Facade();
        facade.sleep();
        System.out.println("=====================");
        facade.wakeUp();
    }
}
