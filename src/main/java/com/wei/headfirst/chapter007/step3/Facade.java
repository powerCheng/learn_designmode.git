package com.wei.headfirst.chapter007.step3;

/**
 * 外观类
 *
 * @author chengwei
 * @date 2019/8/27 15:47
 */
public class Facade {

    private AirConditioner airConditioner;

    private TV tv;

    private Light light;

    private NightLamp nightLamp;

    public Facade() {
        airConditioner = new AirConditioner();
        tv = new TV();
        light = new Light();
        nightLamp = new NightLamp();
    }

    /**
     * 准备睡觉
     */
    public void sleep(){
        System.out.println("准备睡觉");
        // 关闭电视
        tv.off();
        // 关闭电灯
        light.off();
        // 打开小夜灯
        nightLamp.on();
    }

    /**
     * 准备起床
     */
    public void wakeUp(){
        System.out.println("准备起床");
        // 打开电灯
        light.on();
        // 关闭小夜灯
        nightLamp.off();
        // 关闭空调
        airConditioner.off();
    }
}
