package com.wei.headfirst.chapter007.step3;

/**
 * 小夜灯
 *
 * @author chengwei
 * @date 2019/8/27 15:36
 */
public class NightLamp {
    public void on(){
        System.out.println("打开小夜灯");
    }

    public void off(){
        System.out.println("关闭小夜灯");
    }
}
