package com.wei.headfirst.chapter007.step3;

/**
 * 空调
 *
 * @author chengwei
 * @date 2019/8/27 15:29
 */
public class AirConditioner {

    public void on(){
        System.out.println("打开空调");
    }

    public void off(){
        System.out.println("关闭空调");
    }
}
