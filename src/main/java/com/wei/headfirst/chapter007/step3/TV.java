package com.wei.headfirst.chapter007.step3;

/**
 * 电视
 *
 * @author chengwei
 * @date 2019/8/27 15:37
 */
public class TV {
    public void on(){
        System.out.println("打开电视");
    }

    public void off(){
        System.out.println("关闭电视");
    }
}
