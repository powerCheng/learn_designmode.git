package com.wei.headfirst.chapter007.step3;

/**
 * 电灯
 *
 * @author chengwei
 * @date 2019/2/12 9:44
 */
public class Light {
    public void on(){
        System.out.println("打开电灯");
    }

    public void off(){
        System.out.println("关闭电灯");
    }
}
