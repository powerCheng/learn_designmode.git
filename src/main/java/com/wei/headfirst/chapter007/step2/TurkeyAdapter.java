package com.wei.headfirst.chapter007.step2;

import com.wei.headfirst.chapter007.step1.Duck;
import com.wei.headfirst.chapter007.step1.WildTurkey;

/**
 * 适配器, 让火鸡呈现出鸭子的行为
 *
 * @author chengwei
 * @date 2019/2/12 16:32
 */
public class TurkeyAdapter extends WildTurkey implements Duck {

    @Override
    public void quack() {
        gobble();
    }
}
