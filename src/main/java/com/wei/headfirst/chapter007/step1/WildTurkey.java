package com.wei.headfirst.chapter007.step1;

/**
 * 火鸡实现类
 *
 * @author chengwei
 * @date 2019/2/12 16:29
 */
public class WildTurkey implements Turkey{
    @Override
    public void gobble(){
        System.out.println("咕咕叫");
    }

    @Override
    public void fly(){
        System.out.println("短程飞翔");
    }
}
