package com.wei.headfirst.chapter007.step1;

/**
 * 绿头鸭, 具体的实现类
 *
 * @author chengwei
 * @date 2019/2/12 16:30
 */
public class MallardDuck implements Duck {
    @Override
    public void fly() {
        System.out.println("长途飞翔");
    }

    @Override
    public void quack() {
        System.out.println("呱呱叫");
    }
}
