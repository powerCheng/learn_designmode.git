package com.wei.headfirst.chapter007.step1;

/**
 * 鸭子接口, 充当目标的角色(Target)
 *
 * @author chengwei
 * @date 2019/2/12 16:28
 */
public interface Duck {

    /**
     * 呱呱叫
     */
    void quack();

    /**
     * 飞翔
     */
    void fly();
}
