package com.wei.headfirst.chapter007.step1;

/**
 * 适配器, 让火鸡呈现出鸭子的行为
 *
 * @author chengwei
 * @date 2019/2/12 16:32
 */
public class TurkeyAdapter implements Duck {
    private Turkey turkey;

    public TurkeyAdapter(Turkey turkey) {
        this.turkey = turkey;
    }

    @Override
    public void quack() {
        turkey.gobble();
    }


    @Override
    public void fly() {
        turkey.fly();
    }
}
