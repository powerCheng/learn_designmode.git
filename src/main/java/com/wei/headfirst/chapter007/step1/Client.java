package com.wei.headfirst.chapter007.step1;

/**
 * @author chengwei
 * @date 2019/2/12 16:32
 */
public class Client {
    public static void main(String[] args) {
        Turkey turkey = new WildTurkey();
        Duck duck = new TurkeyAdapter(turkey);
        duck.fly();
        duck.quack();
    }
}
