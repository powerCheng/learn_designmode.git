package com.wei.headfirst.chapter007.step1;

/**
 * 火鸡, 充当适配者角色(Adapter)
 *
 * @author chengwei
 * @date 2019/2/12 16:30
 */
public interface Turkey {
    /**
     * 咕咕叫
     */
    void gobble();

    /**
     * 飞翔
     */
    void fly();
}
