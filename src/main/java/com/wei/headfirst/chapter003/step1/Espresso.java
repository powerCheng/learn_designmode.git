package com.wei.headfirst.chapter003.step1;

/**
 * 浓咖啡(一种具体的饮料), 具体构建角色
 *
 * @author chengwei
 * @date 2019-06-10 10:40
 */
public class Espresso extends Beverage {

    public Espresso() {
        super.description = "浓咖啡";
    }

    @Override
    public double cost() {
        return 1.80;
    }
}
