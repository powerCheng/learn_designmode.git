package com.wei.headfirst.chapter003.step1;

/**
 * 糖(一种调料), 具体的装饰器角色
 *
 * @author chengwei
 * @date 2019-06-10 10:42
 */
public class Suger extends Condiment {

    public Suger(Beverage beverage) {
        super(beverage);
    }

    @Override
    public String getDescription() {
        return beverage.getDescription()+", 糖";
    }

    @Override
    public double cost() {
        return 0.16 + beverage.cost();
    }
}
