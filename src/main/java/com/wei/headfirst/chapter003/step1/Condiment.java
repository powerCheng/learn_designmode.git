package com.wei.headfirst.chapter003.step1;

/**
 * 调料抽象类, 抽象装饰器角色
 *
 * @author chengwei
 * @date 2019-06-10 10:37
 */
public abstract class Condiment extends Beverage {

    protected Beverage beverage;

    public Condiment(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public abstract String getDescription() ;
}
