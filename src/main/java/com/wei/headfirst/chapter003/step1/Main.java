package com.wei.headfirst.chapter003.step1;

/**
 * @author chengwei
 * @date 2019-06-10 10:47
 */
public class Main {
    public static void main(String[] args) {
        Beverage beverage = new Espresso();
        System.out.println(beverage.getDescription() + ", $"+ beverage.cost());

        beverage = new Mocha(beverage);
        beverage = new Suger(beverage);
        System.out.println(beverage.getDescription() + ", $"+ beverage.cost());
    }
}
