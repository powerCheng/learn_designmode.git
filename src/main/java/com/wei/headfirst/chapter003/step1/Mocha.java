package com.wei.headfirst.chapter003.step1;

/**
 * 摩卡(一种调料), 具体的装饰器角色
 *
 * @author chengwei
 * @date 2019-06-10 10:42
 */
public class Mocha extends Condiment {

    public Mocha(Beverage beverage) {
        super(beverage);
    }

    @Override
    public String getDescription() {
        return beverage.getDescription()+", 摩卡";
    }

    @Override
    public double cost() {
        return 0.20 + beverage.cost();
    }
}
