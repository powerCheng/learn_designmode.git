package com.wei.headfirst.chapter003.step1;

/**
 * 饮料, 抽象构建角色
 *
 * @author chengwei
 * @date 2019-06-06 18:04
 */
public abstract class Beverage {

    protected String description = "Unknown Beverage";

    public String getDescription(){
        return description;
    }

    public abstract double cost();
}
