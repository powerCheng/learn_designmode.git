package com.wei.headfirst.chapter003.step0;

/**
 * 具体的构建实现类
 *
 * @author chengwei
 * @date 2019-06-06 17:33
 */
public class ConcreteComponent implements Component {
    @Override
    public void methodA() {
        System.out.print("基础消费20元");
    }
}
