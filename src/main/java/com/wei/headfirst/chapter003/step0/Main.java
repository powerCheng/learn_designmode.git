package com.wei.headfirst.chapter003.step0;

/**
 * @author chengwei
 * @date 2019-06-06 17:41
 */
public class Main {
    public static void main(String[] args) {
        Component component = new ConcreteDecoratorB(new ConcreateDecoratorA(new ConcreteComponent()));
        component.methodA();
    }
}
