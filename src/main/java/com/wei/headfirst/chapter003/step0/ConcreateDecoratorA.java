package com.wei.headfirst.chapter003.step0;

/**
 * 具体的装饰类
 *
 * @author chengwei
 * @date 2019-06-06 17:37
 */
public class ConcreateDecoratorA extends Decorator {

    public ConcreateDecoratorA(Component component) {
        super.component = component;
    }

    @Override
    public void methodA() {
        component.methodA();
        System.out.print(", Aoo附加值 30元");
    }
}
