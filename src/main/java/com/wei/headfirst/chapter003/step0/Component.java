package com.wei.headfirst.chapter003.step0;

/**
 * 构建接口, 构建可以单独使用, 也可以被装饰者装饰后使用
 *
 * @author chengwei
 * @date 2019-06-06 17:30
 */
public interface Component {

    void methodA();
}
