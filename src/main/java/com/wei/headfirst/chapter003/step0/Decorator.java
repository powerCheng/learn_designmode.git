package com.wei.headfirst.chapter003.step0;

/**
 * 抽象的装饰类, 实现构建接口, 并持有一个构建对象
 *
 * @author chengwei
 * @date 2019-06-06 17:35
 */
public abstract class Decorator implements Component {

    protected Component component;


}
