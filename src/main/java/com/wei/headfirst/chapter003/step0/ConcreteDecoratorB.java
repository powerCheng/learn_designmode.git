package com.wei.headfirst.chapter003.step0;

/**
 * 具体的装饰类
 *
 * @author chengwei
 * @date 2019-06-06 17:40
 */
public class ConcreteDecoratorB extends Decorator {

    public ConcreteDecoratorB(Component component) {
        super.component = component;
    }

    @Override
    public void methodA() {
        component.methodA();
        System.out.println(", Boo附加值 40元");
    }
}
