package com.wei.headfirst.chapter011.step2;

/**
 * @author chengwei
 * @date 2019/9/2 10:29
 */
public class SubjectImpl implements Subject {
    @Override
    public void sayHi() {
        System.out.println("say hi ...");
    }

    @Override
    public void sayHello(String name, Integer age) {
        System.out.println("hello " + name + ", you are " + age + " years old");
    }


}
