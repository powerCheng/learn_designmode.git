package com.wei.headfirst.chapter011.step2;

import java.lang.reflect.Proxy;

/**
 * @author chengwei
 * @date 2019/9/2 10:34
 */
public class Client {
    public static void main(String[] args) {
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles","true");
        Subject subject = new SubjectImpl();
        Subject subjectProxy = (Subject) Proxy.newProxyInstance(subject.getClass().getClassLoader(), subject.getClass().getInterfaces(), new MyInvocationHandler(subject));
        subjectProxy.sayHello("张三", 18);
    }
}
