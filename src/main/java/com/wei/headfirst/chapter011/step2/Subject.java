package com.wei.headfirst.chapter011.step2;

/**
 * @author chengwei
 * @date 2019/9/2 10:28
 */
public interface Subject {
    void sayHi();

    void  sayHello(String name, Integer age);
}
