package com.wei.headfirst.chapter011.step2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 自定义处理器
 *
 * @author chengwei
 * @date 2019/9/2 10:29
 */
public class MyInvocationHandler implements InvocationHandler {

    private Subject subject;

    public MyInvocationHandler(Subject subject) {
        this.subject = subject;
    }

    /**
     * 处理逻辑
     *
     * @param proxy 调用该方法的代理实例
     * @param method 抽象主题中的方法对象
     * @param args 调用方法时传入的参数列表
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        preRequest();
        method.invoke(subject, args);
        afterRequest();
        return null;
    }

    public void preRequest(){
        System.out.println("before request ...");
    }

    public void afterRequest(){
        System.out.print("after request ...");
    }
}
