package com.wei.headfirst.chapter011.step1;

/**
 * 真实主题
 *
 * @author chengwei
 * @date 2019/9/2 10:01
 */
public class RealSubject implements Subject {
    @Override
    public void request() {
        System.out.println("this is real Subject");
    }
}
