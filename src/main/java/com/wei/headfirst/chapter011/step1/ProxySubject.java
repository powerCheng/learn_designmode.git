package com.wei.headfirst.chapter011.step1;

/**
 * 代理类. 实现了主题接口, 持有一个主题实现对象的引用，可以访问、控制、扩展真实主题的功能
 *
 * @author chengwei
 * @date 2019/9/2 10:01
 */
public class ProxySubject implements Subject {

    private Subject subject;

    public ProxySubject(Subject subject) {
        this.subject = subject;
    }

    /**
     * 扩展真实主题的功能, 在真实主题的访问前和访问后分别干点事情
     */
    @Override
    public void request() {
        preRequest();
        subject.request();
        afterRequest();
    }

    public void preRequest(){
        System.out.println("before request ...");
    }

    public void afterRequest(){
        System.out.println("after request ...");
    }
}
