package com.wei.headfirst.chapter011.step1;

/**
 * @author chengwei
 * @date 2019/9/2 10:03
 */
public class Client {
    public static void main(String[] args) {
        Subject subject = new RealSubject();
        ProxySubject proxySubject = new ProxySubject(subject);
        proxySubject.request();
    }
}
