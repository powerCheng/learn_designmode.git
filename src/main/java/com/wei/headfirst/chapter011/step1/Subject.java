package com.wei.headfirst.chapter011.step1;

/**
 * 抽象主题
 *
 * @author chengwei
 * @date 2019/9/2 10:00
 */
public interface Subject {
    void request();
}
