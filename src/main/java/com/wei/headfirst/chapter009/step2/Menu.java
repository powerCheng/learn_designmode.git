package com.wei.headfirst.chapter009.step2;

import java.util.List;

/**
 * @author chengwei
 * @date 2019/2/14 9:19
 */
public interface Menu {
    default void addChild(Menu menu){
        throw new UnsupportedOperationException();
    }

    default void removeChild(Menu menu){
        throw new UnsupportedOperationException();
    }

    List<Menu> getChildren();

    default String getType(){
        return null;
    }

    List<MenuItem> getMenuItem();

    default List<MenuItem> getMenuItem(String type){
        return null;
    }
}
