package com.wei.headfirst.chapter009.step2;

/**
 * @author chengwei
 * @date 2019/2/14 11:12
 */
public class MenuService {

    private static final MenuService menuService = new MenuService();

    private MenuService() {
    }

    public static MenuService getInstance(){
        return menuService;
    }

    public Menu getBreakfast(){
        Menu breakfast = new GroupMenu("早餐");
        breakfast.addChild(new LeafMenu("豆浆", 8.5));
        breakfast.addChild(new LeafMenu("油条", 5.5));
        breakfast.addChild(new LeafMenu("大饼", 6.5));
        breakfast.addChild(new LeafMenu("包子", 3.5));
        Menu dessert = new GroupMenu("甜点");
        dessert.addChild(new LeafMenu("小儿苏", 3));
        dessert.addChild(new LeafMenu("蛋糕", 3));
        dessert.addChild(new LeafMenu("薯条", 3));
        dessert.addChild(new LeafMenu("薯片", 3));
        breakfast.addChild(dessert);
        return breakfast;
    }

    public Menu getLunch(){
        Menu lunch = new GroupMenu("午餐");
        lunch.addChild(new LeafMenu("小炒肉", 132));
        lunch.addChild(new LeafMenu("鱼香肉丝", 12));
        lunch.addChild(new LeafMenu("西红柿炒蛋", 20));
        lunch.addChild(new LeafMenu("排骨汤", 162));
        lunch.addChild(new LeafMenu("狮子头", 162));
        Menu meal = new GroupMenu("套餐");
        meal.addChild(new LeafMenu("精品套餐", 48));
        meal.addChild(new LeafMenu("普通套餐", 28));
        lunch.addChild(meal);
        return lunch;
    }
}
