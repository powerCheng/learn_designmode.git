package com.wei.headfirst.chapter009.step2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chengwei
 * @date 2019/2/14 10:25
 */
public class GroupMenu implements Menu {
    private String type;
    private List<Menu> children;

    public GroupMenu(String type) {
        this.type = type;
        children = new ArrayList<>();
    }

    @Override
    public List<Menu> getChildren() {
        return children;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public List<MenuItem> getMenuItem() {
        return getMenuItems(children);
    }

    private List<MenuItem> getMenuItems(List<Menu> menus) {
        List<Menu> leafMeaus = getLeafMemu(menus);
        List<MenuItem> list = new ArrayList<>();
        for (Menu leafMeau : leafMeaus) {
            list.addAll(leafMeau.getMenuItem());
        }
        return list;
    }

    @Override
    public void addChild(Menu menu) {
        children.add(menu);
    }

    @Override
    public void removeChild(Menu menu) {
        children.remove(menu);
    }


    public List<Menu> getLeafMemu(List<Menu> menuList) {
        List<Menu> leafMeaus = new ArrayList<>();
        for (Menu menu : menuList) {
            if (menu.getChildren() == null) {
                leafMeaus.add(menu);
            } else {
                leafMeaus.addAll(getLeafMemu(menu.getChildren()));
            }
        }
        return leafMeaus;
    }

    @Override
    public List<MenuItem> getMenuItem(String type) {
        Menu menu = getByType(children, type);
        return getMenuItems(menu.getChildren());
    }

    public Menu getByType(List<Menu> menuList, String type) {
        if(type.equals(this.type)){
            return this;
        }
        for (Menu menu : menuList) {
            if(type.equals(menu.getType())){
                return menu;
            }else if(menu.getChildren() != null){
                Menu menu1 = getByType(menu.getChildren(), type);
                if(null != menu1){
                    return menu1;
                }
            }
        }
        return null;
    }


}
