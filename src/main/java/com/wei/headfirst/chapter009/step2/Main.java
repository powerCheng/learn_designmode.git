package com.wei.headfirst.chapter009.step2;

import static com.wei.util.PrintUtil.line;

/**
 * @author chengwei
 * @date 2019/2/14 10:56
 */
public class Main {
    public static void main(String[] args) {
        MenuService menuService = MenuService.getInstance();
        Menu breakfast = menuService.getBreakfast();
        Menu lunch = menuService.getLunch();

        Menu menu = new GroupMenu("总菜单");
        menu.addChild(breakfast);
        menu.addChild(lunch);

        for (MenuItem menuItem : menu.getMenuItem()) {
            System.out.println(menuItem);
        }
        line("甜点");
        for (MenuItem menuItem : menu.getMenuItem("甜点")) {
            System.out.println(menuItem);
        }
        line("午餐");
        for (MenuItem menuItem : menu.getMenuItem("午餐")) {
            System.out.println(menuItem);
        }
    }
}
