package com.wei.headfirst.chapter009.step2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author chengwei
 * @date 2019/2/14 10:04
 */
public class LeafMenu implements Menu {
    private MenuItem menuItem;

    public LeafMenu(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    public LeafMenu(String name, double price) {
        this.menuItem = new MenuItem(name, price);
    }

    @Override
    public List<Menu> getChildren() {
        return null;
    }

    @Override
    public List<MenuItem> getMenuItem() {
        return new ArrayList<>(Arrays.asList(menuItem));
    }
}
