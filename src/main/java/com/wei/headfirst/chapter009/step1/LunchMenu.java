package com.wei.headfirst.chapter009.step1;

import java.util.Iterator;

/**
 * @author chengwei
 * @date 2019/2/14 9:22
 */
public class LunchMenu implements Menu<MenuItem>{
    private MenuItem[] menus;

    public LunchMenu() {
        menus = new MenuItem[5];
        addMenu(0, "小炒肉", 13);
        addMenu(1, "鱼香肉丝", 16);
        addMenu(2, "西红柿炒蛋", 12);
        addMenu(3, "排骨汤", 16);
        addMenu(4, "狮子头", 16);
    }

    private void addMenu(int index, String name, double price) {
        menus[index] = new MenuItem(name, price);
    }


    @Override
    public Iterator<MenuItem> getMenus() {
        return new ArrayIterator<>(menus);
    }
}
