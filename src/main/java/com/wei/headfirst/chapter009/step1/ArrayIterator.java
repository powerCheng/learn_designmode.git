package com.wei.headfirst.chapter009.step1;

import java.util.Iterator;

/**
 * @author chengwei
 * @date 2019/2/14 9:32
 */
public class ArrayIterator<T> implements Iterator<T> {

    private T[] items;

    private int position;

    public ArrayIterator(T[] items) {
        this.items = items;
        position = 0;
    }

    @Override
    public boolean hasNext() {
        if(position >= items.length || items[position] == null){
            return false;
        }
        return true;
    }

    @Override
    public T next() {
        return items[position++];
    }
}
