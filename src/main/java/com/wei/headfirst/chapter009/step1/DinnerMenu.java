package com.wei.headfirst.chapter009.step1;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author chengwei
 * @date 2019/2/14 9:38
 */
public class DinnerMenu implements Menu<MenuItem>{
    private Map<String, MenuItem> menuItemMap;

    public DinnerMenu() {
        menuItemMap = new HashMap<>();
        addMenu("螃蟹", 44);
        addMenu("龙虾", 55);
        addMenu("海蜇", 66);
        addMenu("鲍鱼", 88);
    }

    public void addMenu(String name, double price) {
        menuItemMap.put(name, new MenuItem(name, price));
    }

    @Override
    public Iterator<MenuItem> getMenus() {
        return menuItemMap.values().iterator();
    }
}
