package com.wei.headfirst.chapter009.step1;

/**
 * @author chengwei
 * @date 2019/2/14 9:56
 */
public class Main {
    public static void main(String[] args) {
        Waitress waitress = new Waitress();
        waitress.printMenuItems();
    }
}
