package com.wei.headfirst.chapter009.step1;

/**
 * 菜单项
 *
 * @author chengwei
 * @date 2019/2/14 9:16
 */
public class MenuItem {
    private String name;
    private double price;

    public MenuItem(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "name=" + name + ", price=" + price;
    }
}
