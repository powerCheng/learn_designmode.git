package com.wei.headfirst.chapter009.step1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 早餐清单
 *
 * @author chengwei
 * @date 2019/2/14 9:15
 */
public class BreakfastMenu implements Menu<MenuItem> {
    private List<MenuItem> list;

    public BreakfastMenu() {
        this.list = new ArrayList<>();
        addMenu("豆浆",8.5);
        addMenu("油条",5.5);
        addMenu("大饼",6.5);
        addMenu("包子",3.5);
    }

    public void addMenu(String name, double price) {
        list.add(new MenuItem(name, price));
    }


    @Override
    public Iterator<MenuItem> getMenus() {
        return list.iterator();
    }
}
