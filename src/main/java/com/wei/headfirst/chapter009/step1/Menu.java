package com.wei.headfirst.chapter009.step1;

import java.util.Iterator;

/**
 * @author chengwei
 * @date 2019/2/14 9:19
 */
public interface Menu<T> {
    Iterator<T> getMenus();
}
