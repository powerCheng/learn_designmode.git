package com.wei.headfirst.chapter009.step1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.wei.util.PrintUtil.line;
import static com.wei.util.PrintUtil.println;

/**
 * 女服务员
 *
 * @author chengwei
 * @date 2019/2/14 9:44
 */
public class Waitress {
    private List<Menu> menus;

    public Waitress() {
        this.menus = new ArrayList<>();
        addMenu(new BreakfastMenu());
        addMenu(new LunchMenu());
        addMenu(new DinnerMenu());
    }

    public void addMenu(Menu menu){
        menus.add(menu);
    }

    public void printMenuItems(){
        for (Menu menu : menus) {
            Iterator<MenuItem> itemIterator = menu.getMenus();
            while (itemIterator.hasNext()){
                MenuItem menuItem = itemIterator.next();
                println(menuItem.toString());
            }
            line();
        }
    }
}
