package com.wei.headfirst.chapter009.step3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author chengwei
 * @date 2019/2/14 10:25
 */
public class GroupMenu implements Menu {
    private String type;
    private List<Menu> children;

    public GroupMenu(String type) {
        this.type = type;
        children = new ArrayList<>();
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void addChild(Menu menu) {
        children.add(menu);
    }

    @Override
    public void removeChild(Menu menu) {
        children.remove(menu);
    }

    @Override
    public boolean isLeaf() {
        return false;
    }

    @Override
    public Iterator<Menu> createIterator() {
        return null;
    }


}
