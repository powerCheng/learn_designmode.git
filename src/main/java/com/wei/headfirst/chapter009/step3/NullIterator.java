package com.wei.headfirst.chapter009.step3;

import java.util.Iterator;

/**
 * @author chengwei
 * @date 2019/2/14 15:52
 */
public class NullIterator implements Iterator {
    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Object next() {
        return null;
    }
}
