package com.wei.headfirst.chapter009.step3;

import java.util.Iterator;
import java.util.List;

/**
 * @author chengwei
 * @date 2019/2/14 9:19
 */
public interface Menu {
    default void addChild(Menu menu){
        throw new UnsupportedOperationException();
    }

    default void removeChild(Menu menu){
        throw new UnsupportedOperationException();
    }


    default String getType(){
        return null;
    }

    boolean isLeaf();

    default MenuItem getData(){
        throw new UnsupportedOperationException();
    }

    Iterator<Menu> createIterator();
}
