package com.wei.headfirst.chapter009.step3;

import lombok.extern.slf4j.Slf4j;


/**
 * @author chengwei
 * @date 2019/2/14 10:56
 */
@Slf4j
public class Main {

    public static void main(String[] args) {
        MenuService menuService = MenuService.getInstance();
        Menu breakfast = menuService.getBreakfast();
        Menu lunch = menuService.getLunch();

        Menu menu = new GroupMenu("总菜单");
        menu.addChild(breakfast);
        menu.addChild(lunch);

//        for (MenuItem menuItem : menu.getMenuItem()) {
//            System.out.println(menuItem);
//        }
//
//        for (MenuItem menuItem : menu.getMenuItem("总菜单")) {
//            System.out.println(menuItem);
//        }
        log.debug("");
    }
}
