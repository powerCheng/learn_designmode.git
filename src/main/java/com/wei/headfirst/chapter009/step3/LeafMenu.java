package com.wei.headfirst.chapter009.step3;

import java.util.Iterator;

/**
 * @author chengwei
 * @date 2019/2/14 10:04
 */
public class LeafMenu implements Menu {
    private MenuItem menuItem;

    public LeafMenu(String name, double price) {
        this.menuItem = new MenuItem(name, price);
    }


    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    public MenuItem getData() {
        return menuItem;
    }

    @Override
    public Iterator<Menu> createIterator() {
        return new NullIterator();
    }
}
