package com.wei.headfirst.chapter009.step3;

import java.util.Iterator;
import java.util.Stack;

/**
 * @author chengwei
 * @date 2019/2/14 15:58
 */
public class GroupIterator implements Iterator {

    private Stack<Menu> stack;

    public GroupIterator(Menu menu) {
        stack = new Stack();
        stack.push(menu);
    }

    @Override
    public boolean hasNext() {
        if(stack.isEmpty()){
            return false;
        }
        Menu menu = stack.peek();
        if(menu instanceof GroupMenu){

        }
        return false;
    }

    @Override
    public Object next() {
        return null;
    }
}
