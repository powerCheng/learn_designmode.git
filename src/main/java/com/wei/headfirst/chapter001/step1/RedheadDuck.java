package com.wei.headfirst.chapter001.step1;

import static com.wei.util.PrintUtil.print;

/**
 * 红头鸭实现类
 *
 * @author chengwei
 * @date 2019/1/29 10:37
 */
public class RedheadDuck extends Duck{

    public RedheadDuck() {
        // 指定具体的行为策略,这些策略也可以通过参数的方式传进来
        setQuackBehavior(new Quack());
        setFlyBehavior(new FlyWithWings());
    }

    @Override
    void display() {
        print("红头鸭");
    }
}
