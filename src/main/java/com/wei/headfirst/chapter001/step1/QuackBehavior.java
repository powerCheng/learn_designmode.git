package com.wei.headfirst.chapter001.step1;

/**
 * 鸭子叫声的策略接口
 *
 * @author chengwei
 * @date 2019/1/29 11:42
 */
public interface QuackBehavior {

    void quack();
}
