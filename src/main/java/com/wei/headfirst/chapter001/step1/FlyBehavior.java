package com.wei.headfirst.chapter001.step1;

/**
 * 飞行策略接口
 *
 * @author chengwei
 * @date 2019/1/29 11:41
 */
public interface FlyBehavior {

    void fly();

}
