package com.wei.headfirst.chapter001.step1;

import static com.wei.util.PrintUtil.println;

/**
 * 具体的飞行策略实现
 *
 * @author chengwei
 * @date 2019/1/29 11:43
 */
public class FlyWithWings implements FlyBehavior {
    @Override
    public void fly() {
        println("用翅膀飞翔");
    }
}
