package com.wei.headfirst.chapter001.step1;

import static com.wei.util.PrintUtil.println;

/**
 * @author chengwei
 * @date 2019/1/29 10:27
 */
public abstract class Duck {
    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    abstract void display();

    public void swim(){
        display();
        println("游泳");
    }

    public void fly(){
        display();
        flyBehavior.fly();
    }

    public void quack(){
        display();
        quackBehavior.quack();
    }

    public FlyBehavior getFlyBehavior() {
        return flyBehavior;
    }

    public void setFlyBehavior(FlyBehavior flyBehavior) {
        this.flyBehavior = flyBehavior;
    }

    public QuackBehavior getQuackBehavior() {
        return quackBehavior;
    }

    public void setQuackBehavior(QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }
}
