package com.wei.headfirst.chapter001.step1;

import static com.wei.util.PrintUtil.print;
import static com.wei.util.PrintUtil.println;

/**
 * 橡皮鸭实现类
 *
 * @author chengwei
 * @date 2019/1/29 10:40
 */
public class RubberDuck extends Duck {

    public RubberDuck() {
        // 指定具体的行为策略, 这些策略也可以通过参数的方式传进来
        setQuackBehavior(new Squeak());
        setFlyBehavior(new FlyNoWay());
    }

    @Override
    void display() {
        print("橡皮鸭");
    }
}
