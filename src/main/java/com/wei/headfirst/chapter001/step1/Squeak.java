package com.wei.headfirst.chapter001.step1;

import static com.wei.util.PrintUtil.println;

/**
 * 具体的叫声策略实现
 *
 * @author chengwei
 * @date 2019/1/29 11:46
 */
public class Squeak implements QuackBehavior {
    @Override
    public void quack() {
        println("吱吱叫");
    }
}
