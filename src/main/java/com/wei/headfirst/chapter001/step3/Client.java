package com.wei.headfirst.chapter001.step3;

import com.google.common.collect.ImmutableList;
import com.wei.headfirst.chapter001.step3.opration.OperationHandler;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author chengwei
 * @date 2020/10/9 14:47
 */
public class Client {
    private static final String LEFT_BRACKET = "(";
    private static final String RIGHT_BRACKET = ")";

    public int optional(String expression) {
        System.out.println(expression);
        while (expression.contains(RIGHT_BRACKET)) {
            // 将括号里的表达式提取出来并计算, 计算后的值替换原先的表达式
            String subExp = StringUtils.substringAfterLast(StringUtils.substringBefore(expression, RIGHT_BRACKET),
                    LEFT_BRACKET);
            String subResult = String.valueOf(optionalWithoutBrackets(subExp));
            expression = StringUtils.replace(expression, LEFT_BRACKET + subExp + RIGHT_BRACKET, subResult);
            System.out.println(expression);
        }
        return optionalWithoutBrackets(expression);
    }

    /**
     * 按照顺序计算
     */
    public int seqCal(List<String> list) {
        int result = Integer.parseInt(list.get(0));
        for (int i = 0; i < list.size() - 2; i += 2) {
            result = OperationHandler.getOperation(list.get(i + 1)).operation(result,
                    Integer.parseInt(list.get(i + 2)));
        }
        return result;
    }

    public int mulDivOption(String expression) {
        return seqCal(split(expression, ImmutableList.of('*', '/')));
    }

    /**
     * 不含括号的四则运算
     */
    public int optionalWithoutBrackets(String expression) {
        Pattern p = Pattern.compile(".*[\\*/].*");
        List<String> list = split(expression, ImmutableList.of('+', '-'));
        for (int i = 0; i < list.size(); i++) {
            if (p.matcher(list.get(i)).find()) {
                list.set(i, String.valueOf(mulDivOption(list.get(i))));
            }
        }
        return seqCal(list);
    }

    public List<String> split(String expression, List<Character> characters) {
        List<String> list = new ArrayList<>();
        int beforeIndex = 0;
        char[] chars = expression.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (characters.contains(chars[i])) {
                list.add(StringUtils.substring(expression, beforeIndex, i));
                list.add(String.valueOf(chars[i]));
                beforeIndex = i + 1;
            }
        }
        list.add(StringUtils.substring(expression, beforeIndex));
        return list;
    }

    public static void main(String[] args) {
        String expression = "(2*(5+3)-6)*(3+2)+(7-2)*2";
        Client client  = new Client();
        System.out.println(client.optional(expression));
    }
}
