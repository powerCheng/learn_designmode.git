package com.wei.headfirst.chapter001.step3;

import java.util.Scanner;

/**
 * @author chengwei
 * @date 2020/10/9 14:44
 */
public class Main {
    public static void main(String[] args) {
        Client client = new Client();
        Scanner scanner = new Scanner(System.in);
        while (true){
            String expression = scanner.next();
            System.out.println(client.optional(expression));
        }
    }
}
