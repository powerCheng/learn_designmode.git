package com.wei.headfirst.chapter001.step3.opration;

public class SubOperation implements Operation {
    @Override
    public int operation(int n1, int n2) {
        return n1 - n2;
    }

    @Override
    public String getType() {
        return "-";
    }
}
