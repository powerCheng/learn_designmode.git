package com.wei.headfirst.chapter001.step3.opration;

public interface Operation {
    int operation(int n1, int n2);

    String getType();
}
