package com.wei.headfirst.chapter001.step0;

/**
 * 具体的策略实现A
 *
 * @author chengwei
 * @date 2019-06-05 15:30
 */
public class AStrategyImpl implements Strategy {

    @Override
    public void operation() {
        System.out.println("执行A算法");
    }
}
