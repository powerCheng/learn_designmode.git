package com.wei.headfirst.chapter001.step0;

/**
 * 具体的策略实现B
 *
 * @author chengwei
 * @date 2019-06-05 15:32
 */
public class BStrategyImpl implements Strategy {
    @Override
    public void operation() {
        System.out.println("执行B算法");
    }
}
