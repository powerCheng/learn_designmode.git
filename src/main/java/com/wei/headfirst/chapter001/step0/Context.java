package com.wei.headfirst.chapter001.step0;

/**
 * 持有策略对象, 运行时可以任意指定具体的策略实现.
 *
 * @author chengwei
 * @date 2019-06-05 15:29
 */
public class Context {
    private Strategy strategy;

    /**
     * 不同的策略实现具有不同的操作
     */
    public void opration(){
        strategy.operation();
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
