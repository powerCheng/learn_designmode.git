package com.wei.headfirst.chapter001.step0;

import static com.wei.util.PrintUtil.line;

/**
 * @author chengwei
 * @date 2019-06-10 9:21
 */
public class Main {
    public static void main(String[] args) {
        Context context = new Context();
        context.setStrategy(new AStrategyImpl());
        context.opration();
        line();
        context.setStrategy(new BStrategyImpl());
        context.opration();
    }
}
