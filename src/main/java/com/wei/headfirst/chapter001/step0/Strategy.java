package com.wei.headfirst.chapter001.step0;

/**
 * 策略接口
 *
 * @author chengwei
 * @date 2019-06-05 15:30
 */
public interface Strategy {

    void operation();
}
