package com.wei.headfirst.chapter006.step5;

import com.wei.headfirst.chapter006.step4.*;

import static com.wei.util.PrintUtil.line;

/**
 * @author chengwei
 * @date 2019/2/12 11:07
 */
public class Main {
    public static void main(String[] args) {

        Light livingRoomLight = new Light("Living Room");
        Light kitchenLight = new Light("Kitchen");
        Stereo stereo = new Stereo("Living Room");
        CellingFan fan = new CellingFan("Living Room");

        LightOnCommand livingRoomLightOnCommand = new LightOnCommand(livingRoomLight);
        LightOnCommand kitchenLightOnCommand = new LightOnCommand(kitchenLight);
        LightOffCommand livingRoomLightOffCommand = new LightOffCommand(livingRoomLight);
        LightOffCommand kitchenLightOffCommand = new LightOffCommand(kitchenLight);
        StereoOnCommand stereoOnCommand = new StereoOnCommand(stereo);
        StereoOffCommand stereoOffCommand = new StereoOffCommand(stereo);
        CellingFanHightCommand cellingFanHightCommand = new CellingFanHightCommand(fan);
        CellingFanOffCommand cellingFanOffCommand = new CellingFanOffCommand(fan);

        Command[] onCommand = {livingRoomLightOnCommand, kitchenLightOnCommand, stereoOnCommand, cellingFanHightCommand};
        Command[] offCommand = {livingRoomLightOffCommand, kitchenLightOffCommand, stereoOffCommand, cellingFanOffCommand};
        MacroCommand macroOnCommand = new MacroCommand(onCommand);
        MacroCommand macroOffCommand = new MacroCommand(offCommand);
        macroOnCommand.execute();
        line();
        macroOffCommand.execute();

    }

}
