package com.wei.headfirst.chapter006.step5;

import com.wei.headfirst.chapter006.step4.Command;

/**
 * @author chengwei
 * @date 2019/2/12 15:31
 */
public class MacroCommand implements Command {
    private Command[] commands;

    public MacroCommand(Command[] commands) {
        this.commands = commands;
    }

    @Override
    public void execute() {
        for (Command command : commands) {
            command.execute();
        }
    }

    @Override
    public void undo() {
        for (Command command : commands) {
            command.undo();
        }
    }
}
