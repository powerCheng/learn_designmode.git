package com.wei.headfirst.chapter006.step4;

/**
 * @author chengwei
 * @date 2019/2/12 9:45
 */
public interface Command {
    void execute();

    void undo();
}
