package com.wei.headfirst.chapter006.step4;

/**
 * 风扇
 *
 * @author chengwei
 * @date 2019/2/12 11:02
 */
public class CellingFan {
    private String name;
    public static final int HIGHT = 3;
    public static final int MEDIUM = 2;
    public static final int LOW = 1;
    public static final int STOP = 0;
    private int speed;

    public CellingFan(String name) {
        this.name = name;
        speed = STOP;
    }

    public void hight() {
        speed = HIGHT;
        System.out.println("打开高档");
    }

    public void medium() {
        speed = MEDIUM;
        System.out.println("打开中档");
    }

    public void low() {
        speed = LOW;
        System.out.println("打开低档");
    }

    public void off() {
        speed = STOP;
        System.out.println("关闭风扇");
    }

    public int getSpeed(){
        return speed;
    }
}
