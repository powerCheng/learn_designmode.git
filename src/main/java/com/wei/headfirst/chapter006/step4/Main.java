package com.wei.headfirst.chapter006.step4;

/**
 * @author chengwei
 * @date 2019/2/12 11:07
 */
public class Main {
    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();

        Light livingRoomLight = new Light("Living Room");
        Light kitchenLight = new Light("Kitchen");
        Stereo stereo = new Stereo("Living Room");
        CellingFan fan = new CellingFan("Living Room");

        LightOnCommand livingRoomLightOnCommand = new LightOnCommand(livingRoomLight);
        LightOnCommand kitchenLightOnCommand = new LightOnCommand(kitchenLight);
        LightOffCommand livingRoomLightOffCommand = new LightOffCommand(livingRoomLight);
        LightOffCommand kitchenLightOffCommand = new LightOffCommand(kitchenLight);
        StereoOnCommand stereoOnCommand = new StereoOnCommand(stereo);
        StereoOffCommand stereoOffCommand = new StereoOffCommand(stereo);
        CellingFanHightCommand cellingFanHightCommand = new CellingFanHightCommand(fan);
        CellingFanLowCommand cellingFanLowCommand = new CellingFanLowCommand(fan);
        CellingFanOffCommand cellingFanOffCommand = new CellingFanOffCommand(fan);

        remoteControl.setCommand(0, livingRoomLightOnCommand, livingRoomLightOffCommand);
        remoteControl.setCommand(1, kitchenLightOnCommand, kitchenLightOffCommand);
        remoteControl.setCommand(2, stereoOnCommand, stereoOffCommand);
        remoteControl.setCommand(3, cellingFanHightCommand, cellingFanOffCommand);
        remoteControl.setCommand(4, cellingFanLowCommand, cellingFanOffCommand);

        System.out.println(remoteControl);

        remoteControl.onButtonWasPressed(0);
        remoteControl.undoButtonWasPressed();
        remoteControl.onButtonWasPressed(1);
        remoteControl.undoButtonWasPressed();
        remoteControl.onButtonWasPressed(2);
        remoteControl.undoButtonWasPressed();
        remoteControl.onButtonWasPressed(3);
        remoteControl.onButtonWasPressed(4);
        remoteControl.undoButtonWasPressed();

    }

}
