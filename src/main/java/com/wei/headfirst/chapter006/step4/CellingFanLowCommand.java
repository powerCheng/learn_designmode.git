package com.wei.headfirst.chapter006.step4;


/**
 * @author chengwei
 * @date 2019/2/12 11:11
 */
public class CellingFanLowCommand implements Command {
    private CellingFan cellingFan;
    private CellingFanUndo cellingFanUndo;
    private int speed;

    public CellingFanLowCommand(CellingFan cellingFan) {
        this.cellingFan = cellingFan;
        cellingFanUndo = CellingFanUndo.getInstance();

    }

    @Override
    public void execute() {
        speed = cellingFan.getSpeed();
        cellingFan.low();
    }

    @Override
    public void undo() {
        cellingFanUndo.undo(speed, cellingFan);
    }
}