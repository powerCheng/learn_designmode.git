package com.wei.headfirst.chapter006.step4;

/**
 * 音响
 *
 * @author chengwei
 * @date 2019/2/12 11:03
 */
public class Stereo {
    private String name;

    public Stereo(String name) {
        this.name = name;
    }

    public void on() {
        System.out.println(name + " 打开音响");
    }

    public void putCD() {
        System.out.println(name + " 放入光盘");
    }

    public void setVolume(int volume) {
        System.out.println(name + " 音量调至" + volume);
    }

    public void off(){
        System.out.println(name + " 关闭音响");
    }
}
