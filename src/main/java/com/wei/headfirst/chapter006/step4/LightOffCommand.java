package com.wei.headfirst.chapter006.step4;


/**
 * @author chengwei
 * @date 2019/2/12 11:11
 */
public class LightOffCommand implements Command {
    private Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.off();
    }

    @Override
    public void undo() {
        light.on();
    }
}
