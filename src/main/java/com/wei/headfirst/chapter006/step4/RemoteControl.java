package com.wei.headfirst.chapter006.step4;


/**
 * @author chengwei
 * @date 2019/2/12 10:34
 */
public class RemoteControl {
    private Command[] onCommands;
    private Command[] offCommands;
    private Command undoCommand;

    public RemoteControl() {
        this.onCommands = new Command[7];
        this.offCommands = new Command[7];
        for (int i = 0; i < 7; i++) {
            onCommands[i] = new NoCommand();
            offCommands[i] = new NoCommand();
        }
        undoCommand = new NoCommand();
    }

    public void onButtonWasPressed(int index) {
        onCommands[index].execute();
        undoCommand = onCommands[index];
    }

    public void offButtonWasPressed(int index) {
        offCommands[index].execute();
        undoCommand = offCommands[index];
    }

    public void undoButtonWasPressed(){
        undoCommand.undo();
    }

    public void setCommand(int index, Command onCommand, Command offCommand) {
        onCommands[index] = onCommand;
        offCommands[index] = offCommand;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("-------RemoteControl-------\n");
        stringBuilder.append("slot 0 ").append(onCommands[0].getClass().getSimpleName()+" ").append(offCommands[0].getClass().getSimpleName()+"\n");
        stringBuilder.append("slot 1 ").append(onCommands[1].getClass().getSimpleName()+" ").append(offCommands[1].getClass().getSimpleName()+"\n");
        stringBuilder.append("slot 2 ").append(onCommands[2].getClass().getSimpleName()+" ").append(offCommands[2].getClass().getSimpleName()+"\n");
        stringBuilder.append("slot 3 ").append(onCommands[3].getClass().getSimpleName()+" ").append(offCommands[3].getClass().getSimpleName()+"\n");
        stringBuilder.append("slot 4 ").append(onCommands[4].getClass().getSimpleName()+" ").append(offCommands[4].getClass().getSimpleName()+"\n");
        stringBuilder.append("slot 5 ").append(onCommands[5].getClass().getSimpleName()+" ").append(offCommands[5].getClass().getSimpleName()+"\n");
        stringBuilder.append("slot 6 ").append(onCommands[6].getClass().getSimpleName()+" ").append(offCommands[6].getClass().getSimpleName()+"\n");
        return stringBuilder.toString();
    }
}
