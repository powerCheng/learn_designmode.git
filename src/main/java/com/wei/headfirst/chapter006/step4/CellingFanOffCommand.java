package com.wei.headfirst.chapter006.step4;


/**
 * @author chengwei
 * @date 2019/2/12 11:12
 */
public class CellingFanOffCommand implements Command {
    private CellingFan cellingFan;
    private CellingFanUndo cellingFanUndo;
    private int speed;

    public CellingFanOffCommand(CellingFan cellingFan) {
        this.cellingFan = cellingFan;
        cellingFanUndo = CellingFanUndo.getInstance();
    }

    @Override
    public void execute() {
        speed = cellingFan.getSpeed();
        cellingFan.off();
    }

    @Override
    public void undo() {
        cellingFanUndo.undo(speed, cellingFan);
    }
}
