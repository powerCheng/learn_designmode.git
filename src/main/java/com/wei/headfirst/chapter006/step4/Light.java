package com.wei.headfirst.chapter006.step4;

/**
 * 电灯
 *
 * @author chengwei
 * @date 2019/2/12 9:44
 */
public class Light {
    private String name;

    public Light(String name) {
        this.name = name;
    }

    public void on(){
        System.out.println(name + " 打开电灯");
    }

    public void off(){
        System.out.println(name + " 关闭电灯");
    }
}
