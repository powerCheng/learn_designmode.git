package com.wei.headfirst.chapter006.step4;

/**
 * @author chengwei
 * @date 2019/2/12 14:30
 */
public class CellingFanUndo {
    private static CellingFanUndo cellingFanUndo;

    private CellingFanUndo() {
    }

    public static CellingFanUndo getInstance(){
        if(null == cellingFanUndo){
            synchronized (CellingFanUndo.class){
                if(null == cellingFanUndo){
                    cellingFanUndo = new CellingFanUndo();
                }
            }
        }
        return cellingFanUndo;
    }

    public void undo(int speed, CellingFan cellingFan) {
        if (speed == CellingFan.HIGHT) {
            cellingFan.hight();
        } else if (speed == CellingFan.MEDIUM) {
            cellingFan.medium();
        } else if (speed == CellingFan.LOW) {
            cellingFan.low();
        } else if (speed == CellingFan.STOP) {
            cellingFan.off();
        }
    }
}
