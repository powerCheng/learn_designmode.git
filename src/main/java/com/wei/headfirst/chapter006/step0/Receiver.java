package com.wei.headfirst.chapter006.step0;

/**
 * 命令最终的接收者, 真正负责执行命令请求的类
 *
 * @author chengwei
 * @date 2019-06-05 16:55
 */
public class Receiver {

    public void action1(){
        System.out.println("打开空调");
    }

    public void action2(){
        System.out.println("调低空调温度");
    }

    public void action3() {
        System.out.println("关闭空调");
    }
}
