package com.wei.headfirst.chapter006.step0;

/**
 * 命令接口
 *
 * @author chengwei
 * @date 2019-06-05 16:52
 */
public interface Command {
    void execute();
}
