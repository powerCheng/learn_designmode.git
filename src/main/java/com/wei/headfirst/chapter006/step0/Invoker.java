package com.wei.headfirst.chapter006.step0;

/**
 * 命令的请求者
 *
 * @author chengwei
 * @date 2019-06-05 16:53
 */
public class Invoker {

    private Command command;

    public void action(){
        command.execute();
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }
}
