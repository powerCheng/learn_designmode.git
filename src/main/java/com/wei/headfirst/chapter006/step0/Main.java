package com.wei.headfirst.chapter006.step0;

/**
 * @author chengwei
 * @date 2019-06-05 17:01
 */
public class Main {
    public static void main(String[] args) {
        Invoker invoker = new Invoker();
        Receiver receiver = new Receiver();

        ACommandImpl command1 = new ACommandImpl();
        command1.setReceiver(receiver);
        invoker.setCommand(command1);
        invoker.action();

//        BCommandImpl command2 = new BCommandImpl();
//        command2.setReceiver(receiver);
//        invoker.setCommand(command2);
//        invoker.action();
    }
}
