package com.wei.headfirst.chapter006.step0;

/**
 * 具体的命令实现类
 *
 * @author chengwei
 * @date 2019-06-05 16:55
 */
public class ACommandImpl implements Command {

    private Receiver receiver;

    @Override
    public void execute() {
        receiver.action1();
        receiver.action2();
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }
}
