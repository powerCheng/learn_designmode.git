package com.wei.headfirst.chapter006.step0;

/**
 * @author chengwei
 * @date 2019-06-05 17:00
 */
public class BCommandImpl implements Command {

    private Receiver receiver;

    @Override
    public void execute() {
        receiver.action3();
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }
}
