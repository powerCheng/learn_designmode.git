package com.wei.headfirst.chapter006.step1;

/**
 * @author chengwei
 * @date 2019/2/12 9:48
 */
public class Main {
    public static void main(String[] args) {
        Light light = new Light();
        Command command = new LightOnCommand(light);

        SampleRemoteControl remote = new SampleRemoteControl();
        remote.setCommand(command);
        remote.buttonWasPressed();
    }
}
