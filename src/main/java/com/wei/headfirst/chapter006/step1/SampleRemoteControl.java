package com.wei.headfirst.chapter006.step1;

/**
 * @author chengwei
 * @date 2019/2/12 9:47
 */
public class SampleRemoteControl {
    private Command command;


    public void buttonWasPressed(){
        command.execute();
    }

    public void setCommand(Command command) {
        this.command = command;
    }
}
