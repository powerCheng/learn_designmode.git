package com.wei.headfirst.chapter006.step1;

/**
 * @author chengwei
 * @date 2019/2/12 9:45
 */
public class LightOnCommand implements Command {

    private Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.on();
    }
}
