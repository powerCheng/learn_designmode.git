package com.wei.headfirst.chapter002.step2;

import java.util.Observable;

import static com.wei.util.PrintUtil.line;

/**
 * @author chengwei
 * @date 2019/1/30 13:53
 */
public class Main {
    public static void main(String[] args) {
        WeatherData data =  new WeatherData();

        AooObserver a = new AooObserver(data);
        BooObserver b = new BooObserver(data);

        data.setMeasurements(18, 32, 44);
        line();
        data.setMeasurements(19, 31, 46);
        line("delete b");
        data.deleteObserver(b);
        data.setMeasurements(20, 33, 45);
        line("add b");
        data.addObserver(b);
        data.setMeasurements(18, 31, 44);

    }
}
