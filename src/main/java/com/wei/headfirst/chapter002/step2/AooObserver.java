package com.wei.headfirst.chapter002.step2;

import com.wei.headfirst.chapter002.step1.Display;

import java.text.MessageFormat;
import java.util.Observable;
import java.util.Observer;

import static com.wei.util.PrintUtil.println;

/**
 * @author chengwei
 * @date 2019/1/30 11:16
 */
public class AooObserver implements Observer, Display {
    private double temperature;
    private double humitidy;
    private double pressure;

    public AooObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData data = (WeatherData) o;
            temperature = data.getTemperature();
            humitidy = data.getHumitidy();
            pressure = data.getPressure();
            display();
        }
    }

    @Override
    public void display() {
        println(MessageFormat.format("当前温度: {0}, 当前湿度: {1}, 当前压力: {2}", temperature, humitidy, pressure));
    }
}
