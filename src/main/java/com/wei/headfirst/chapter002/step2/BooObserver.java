package com.wei.headfirst.chapter002.step2;

import com.wei.headfirst.chapter002.step1.Display;
import org.apache.commons.lang.math.NumberUtils;

import java.text.MessageFormat;
import java.util.Observable;
import java.util.Observer;

import static com.wei.util.PrintUtil.println;

/**
 * @author chengwei
 * @date 2019/1/30 13:47
 */
public class BooObserver implements Observer, Display {
    private double maxTemperature;
    private double maxHumitidy;
    private double maxPressure;

    public BooObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData data = (WeatherData) o;
            maxTemperature = max(maxTemperature, data.getTemperature());
            maxHumitidy = max(maxHumitidy, data.getHumitidy());
            maxPressure = max(maxPressure, data.getPressure());
            display();
        }
    }

    private double max(double a, double b) {
        if (a >= b) {
            return a;
        } else {
            return b;
        }
    }

    @Override
    public void display() {
        println(MessageFormat.format("最高温度: {0}, 最高湿度: {1}, 最高压力: {2}", maxTemperature, maxHumitidy, maxPressure));
    }
}