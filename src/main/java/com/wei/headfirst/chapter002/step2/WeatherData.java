package com.wei.headfirst.chapter002.step2;

import java.util.Observable;

/**
 * @author chengwei
 * @date 2019/1/30 11:58
 */
public class WeatherData extends Observable {
    private double temperature;
    private double humitidy;
    private double pressure;

    public void measurementsChange(){
        setChanged();
        notifyObservers();
    }

    public void setMeasurements(double temperature, double humitidy, double pressure){
        this.temperature = temperature;
        this.humitidy = humitidy;
        this.pressure = pressure;
        measurementsChange();
    }

    public double getTemperature() {
        return temperature;
    }

    public double getHumitidy() {
        return humitidy;
    }

    public double getPressure() {
        return pressure;
    }
}
