package com.wei.headfirst.chapter002.step1;

import static com.wei.util.PrintUtil.line;

/**
 * @author chengwei
 * @date 2019/1/30 10:58
 */
public class Main {
    public static void main(String[] args) {
        Subject subject = new WeatherData();

        Observer aoo = new AooOberver(subject);
        new BooOberver(subject);

        subject.setData(11, 39, 22);
        line();
        subject.setData(12, 30, 26);
        line("remove aoo");
        subject.removeObserver(aoo);
        subject.setData(13, 30, 26);
        line("add coo");
        new CooOberver(subject);
        subject.setData(14, 30, 26);
    }
}