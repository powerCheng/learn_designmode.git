package com.wei.headfirst.chapter002.step1;


import java.text.MessageFormat;

import static com.wei.util.PrintUtil.println;

/**
 * @author chengwei
 * @date 2019/1/30 10:43
 */
public class CooOberver implements Observer, Display {

    private Subject subject;

    public CooOberver(Subject subject) {
        this.subject = subject;
        subject.registerObserver(this);
    }

    @Override
    public void update(Subject subject) {
        this.subject = subject;
        display();
    }

    @Override
    public void display() {
        println(MessageFormat.format("温度cc: {0}, 湿度cc: {1}, 压力cc: {2}", subject.getTemperature(), subject.getHumitidy(),
                subject.getPressure()));
    }

}
