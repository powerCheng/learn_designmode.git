package com.wei.headfirst.chapter002.step1;

/**
 * @author chengwei
 * @date 2019/1/30 10:41
 */
public interface Observer {
    void update(Subject subject);
}
