package com.wei.headfirst.chapter002.step1;

/**
 * @author chengwei
 * @date 2019/1/30 10:37
 */
public interface Subject {
    void registerObserver(Observer observer);

    void removeObserver(Observer observer);

    void notifyObservers();

    double getTemperature();

    double getHumitidy();

    int getPressure() ;

    void setData(double temperature, double humitidy, int pressure);
}
