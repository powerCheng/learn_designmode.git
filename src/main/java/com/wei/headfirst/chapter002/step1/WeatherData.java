package com.wei.headfirst.chapter002.step1;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chengwei
 * @date 2019/1/30 10:38
 */
public class WeatherData implements Subject {
    private double temperature;
    private double humitidy;
    private int pressure;
    private List<Observer> observerList = new ArrayList<>();

    @Override
    public void registerObserver(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observerList) {
            observer.update(this);
        }
    }

    @Override
    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public double getHumitidy() {
        return humitidy;
    }

    public void setHumitidy(double humitidy) {
        this.humitidy = humitidy;
    }

    @Override
    public int getPressure() {
        return pressure;
    }

    @Override
    public void setData(double temperature, double humitidy, int pressure) {
        this.temperature = temperature;
        this.humitidy = humitidy;
        this.pressure = pressure;
        notifyObservers();
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public List<Observer> getObserverList() {
        return observerList;
    }

    public void setObserverList(List<Observer> observerList) {
        this.observerList = observerList;
    }
}
