package com.wei.headfirst.chapter002.step1;


import java.text.MessageFormat;

import static com.wei.util.PrintUtil.println;

/**
 * @author chengwei
 * @date 2019/1/30 10:43
 */
public class AooOberver implements Observer, Display {

    private Subject subject;

    public AooOberver(Subject subject) {
        this.subject = subject;
        subject.registerObserver(this);
    }

    @Override
    public void update(Subject subject) {
        this.subject = subject;
        display();
    }

    @Override
    public void display() {
        println(MessageFormat.format("温度aa: {0}, 湿度aa: {1}, 压力aa: {2}", subject.getTemperature(), subject.getHumitidy(),
                subject.getPressure()));
    }

}
