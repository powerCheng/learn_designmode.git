package com.wei.headfirst.chapter002.step1;

/**
 * @author chengwei
 * @date 2019/1/30 10:46
 */
public interface Display {
    void display();
}
